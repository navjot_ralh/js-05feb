var a=10;
var b=20;
function add(){
    return a+b;                                         //return is the final statement of a function
    document.write('Hello');                            //after that no line will execute
}
document.write(4*add());

//parameterized function
function addition(x,y,z){
    return x+y+z;
}
function name(a){
    document.write('<br/>','<h1> My name is '+a+'</h1>');
}

function adding(x,y){
    return x+y;
}